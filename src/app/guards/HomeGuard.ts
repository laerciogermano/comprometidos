import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { TokenService } from '../services/TokenService';
import { Config } from '../constants/Config';
import { StoreApi } from '../services/StoreApi';
import { StoreService } from '../services/StoreService';
import { MessageService } from 'primeng/api';
import * as messages from "../constants/Messages.json";
import { ActionService } from '../services/ActionService';
import { ApiService } from '../services/ApiService';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {

  constructor(
    private tokenService: TokenService,
    private storeApi: StoreApi,
    private store: StoreService,
    private action: ActionService,
    private api: ApiService
  ) { }

  async canActivate() {
    const queryToken = this.tokenService.getQueryToken();
    const localToken = this.tokenService.getLocalToken();
    const token = queryToken || localToken; // querytoken tem prioridade

    // salva token
    this.api.setHeaders(token);

    // Espera baixar os dados do usuário
    await this.getAppData();

    // Espera um pouco para mostrar o loading
    await this.sleep(500);

    // Regra 1: Se tiver query token, salva na storage e permite acesso
    if (queryToken) {
      this.tokenService.setLocalToken(queryToken);
      return true;
    }

    // Regra 2: Se não tiver query ou local token, redireciona para login
    if (!localToken) {
      this.action.logout();
      return false;
    }

    // Regra 3: Caso não possua querytoken, mas possua localToken, permite o acesso
    return true;
  }

  public async getAppData() {
    try {
      const res: any = await this.storeApi.getUsuario();
      
      const login = res.usuario.login;
      const codAgente = res.usuario.codAgente;
      const codUnidade = await this.api.getCodUnidade(codAgente);

      const date = new Date();
      const ano = date.getFullYear();

      this.store.changeAno(ano);

      this.store.changeCodAgente(codAgente);
      this.store.changeCodUnidade(codUnidade);
      this.storeApi.getImagemUsuario(login);
      this.storeApi.getComprometidos();
      this.storeApi.getProjetosAcoes(ano, 1);
      this.storeApi.getTiposDespesas();
      this.storeApi.getAnosRefs();
    } catch (e) {
      this.action.logout();
    }
  }

  private sleep(time) {
    return new Promise(resolve => {
      setTimeout(resolve, time)
    });
  }
}