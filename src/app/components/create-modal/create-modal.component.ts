import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/ApiService';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { StoreService } from 'src/app/services/StoreService';
import { MessageService } from 'primeng/api';
import * as messages from "../../constants/Messages.json";
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import { StoreApi } from 'src/app/services/StoreApi';
import { EventService } from 'src/app/services/Event';

@Component({
  selector: 'app-create-modal',
  templateUrl: './create-modal.component.html',
  styleUrls: ['./create-modal.component.scss']
})
export class CreateModalComponent implements OnInit {
  public form;
  public tipos = [];
  public mesesRef = [
    { name: 'Janeiro' },
    { name: 'Fevereiro' },
    { name: 'Março' },
    { name: 'Abril' },
    { name: 'Maio' },
    { name: 'Junho' },
    { name: 'Julho' },
    { name: 'Agosto' },
    { name: 'Setembro' },
    { name: 'Outubro' },
    { name: 'Novembro' },
    { name: 'Dezembro' },
  ];
  public maskDoc;
  public maskCPF = '999.999.999-99';
  public maskCNPJ = '99.999.999/9999-99';
  public submited = false;

  public numberMask = createNumberMask({
    prefix: 'R$ ',
    suffix: '', // This will put the dollar sign at the end, with a space.
    allowDecimal: true,
    decimalSymbol: ',',
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: '.',
    requireDecimal: false,
    allowLeadingZeroes: true
  });

  public result = [];

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private store: StoreService,
    private toast: MessageService,
    private storeApi: StoreApi,
    private event: EventService
  ) { }

  ngOnInit() {
    this.store.source.subscribe(store => {
      store = this.clone(store);

      this.tipos = store.tiposDespesas;
    });

    this.form = this.formBuilder.group({
      centroCusto: new FormControl(null, Validators.required),
      tipoSistema: new FormControl(null, Validators.required),
      tipoDoc: new FormControl('1', Validators.required),
      doc: new FormControl(null, Validators.required),
      mes: new FormControl(null, Validators.required),
      valorComprometido: new FormControl(null, Validators.required),
      nomeAgente: new FormControl(null),
      observacao: new FormControl('')
    });

    this.setMaskDoc(this.form.value);

    this.form.valueChanges.subscribe(val => this.setMaskDoc(val));

    this.event.onModalCreateOpen.subscribe(() => {
      this.submited = false;
    });
  }

  private setMaskDoc(val) {
    this.maskDoc = val.tipoDoc == '2'
      ? this.maskCPF
      : this.maskCNPJ;
  }

  private clone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  public async search(event) {
    try {
      const query = event.query;
      const ano = this.store.source.value.ano;

      const res: any = await this.api.buscaCentroCusto(ano, query);

      this.result = res || [];
    } catch (e) {
      this.result = [];
    }
  }

  public closeModal() {
    const modal = document.querySelector('#modal-create');
    // document.body.style.overflow = 'hidden';
    modal.classList.remove('visible');
    this.form.reset({ tipoDoc: '1' });
  }


  public async submit() {
    this.submited = true;

    console.log(this.form.value);

    if (!this.form.valid)
      return this.toast.add({ severity: 'error', detail: messages['msg.empty.fields'] });

    try {
      const values = this.form.value;
      const vComprometido = this.parseCurrency(values.valorComprometido);

      await this.api.criarComprometido({
        centroCusto: values.centroCusto,
        tipoSistema: values.tipoSistema,
        tipoDoc: values.tipoDoc,
        doc: values.doc,
        nomeAgente: values.nomeAgente,
        mesRef: values.mes.name,
        observacao: values.observacao,
        valorComprometido: vComprometido,
        codTipo: values.tipoSistema.codTipo,
        ano: this.store.value.ano,
        codAgente: this.store.value.codAgente
      });

      this.storeApi.getComprometidos();

      this.toast.add({
        severity: 'success',
        detail: messages['msg.create.success']
      });

      this.form.reset({ tipoDoc: '1' });
      this.closeModal();
    } catch (e) {
      console.log(e);
      this.toast.add({ severity: 'error', detail: messages['msg.create.fail'] });
    }
  }

  private parseCurrency(value) {
    return value;
  }
}
