import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/Event';
import { ApiService } from 'src/app/services/ApiService';
import { StoreApi } from 'src/app/services/StoreApi';
import * as messages from "../../constants/Messages.json";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-remove-modal',
  templateUrl: './remove-modal.component.html',
  styleUrls: ['./remove-modal.component.scss']
})
export class RemoveModalComponent implements OnInit {
  public comprometido;

  constructor(
    private event: EventService,
    private api: ApiService,
    private storeApi: StoreApi,
    private toast: MessageService
  ) { }

  ngOnInit() {
    this.event.onModalDeleteOpen.subscribe(item => {
      this.comprometido = item;
    });
  }

  public closeModal(){
    const modal = document.querySelector('#modal-remove');
    // document.body.style.overflow = 'visible';
    modal.classList.remove('visible');
  }

  public async delete(){
    try{
      await this.api.deleteComprometidos('934303', this.comprometido.codComprometido);

      this.storeApi.getComprometidos();

      this.toast.add({
        severity: 'success',
        detail: messages['msg.delete.success']
      });
    }catch(e){
      return this.toast.add({ severity: 'error', detail: messages['msg.delete.fail'] });
    }
    this.closeModal();
  }

}
