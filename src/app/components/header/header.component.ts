import { Component, OnInit, HostListener } from '@angular/core';
import { StoreApi } from 'src/app/services/StoreApi';
import { StoreService } from 'src/app/services/StoreService';
import { ActionService } from 'src/app/services/ActionService';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public session;
  public loading: Boolean = true;
  public helpVisible = false;
  public notificationVisible = false;
  public accountVisible = false;

  constructor(
    private store: StoreService,
    private action: ActionService
  ) { }

  async ngOnInit() {
    this.store.source.subscribe(store => {
      this.session = store.usuario;

      try{
        this.session.usuario.nome = this.session.usuario.nome.toLowerCase();
      }catch(e){}
      
      this.loading = store.loading;
    });
  }

  public resetTour(){
    this.store.changeTourState({
      column: false,
      favorite: false
    });
  }

  public showHelp(){
    this.helpVisible = true;
  }

  public showNotification(){
    this.notificationVisible = true;
  }

  public showAccount(){
    this.accountVisible = true;
  }

  public closeHelp(){
    this.helpVisible = false;
  }

  private sleep(time){
    return new Promise(resolve => {
      setTimeout(resolve, time);
    });
  }

  public logout(){
    this.action.logout();
  }

  @HostListener('document:click', ['$event'])
  onClickOutside($event) {
    const $help = document.querySelector('.item.help');
    const $noti = document.querySelector('.item.notification');
    const $user = document.querySelector('.item.account');

    if ($help && !$help.contains($event.target)) {
      this.helpVisible = false;
    }
    
    if ($noti && !$noti.contains($event.target)){
      this.notificationVisible = false;
    }

    if ($user && !$user.contains($event.target)){
      this.accountVisible = false;
    }
  }

}
