import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { EventService } from 'src/app/services/Event';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {
  public tourVisible = false;

  constructor(
    public store: StoreService,
    private event: EventService
  ) { }

  ngOnInit() {}

  public openItemTour(){
    this.tourVisible = true;
  }

  public closeTour(){
    this.store.changeTourState({ 'column': true });
    this.tourVisible = false;
  } 

  public openModalCreate(){
    const modal = document.querySelector('#modal-create');
    modal.classList.add('visible');
    // document.body.style.overflow = 'hidden';

    this.event.onModalCreateOpen.emit();
  }

  public openModalColumn(){
    this.event.onModalColumnOpen.emit();
  }

}
