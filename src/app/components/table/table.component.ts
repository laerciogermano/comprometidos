import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { TableComponentHelper } from './table.helper';
import { EventService } from 'src/app/services/Event';
import { StoreApi } from 'src/app/services/StoreApi';
import { ActionService } from 'src/app/services/ActionService';
import { MessageService } from 'primeng/api';
import * as messages from "../../constants/Messages.json";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public rows;
  public first;
  
  public totalComprometido;
  public totalRealizado;
  public totalSaldo;

  public items;
  public columns = {};

  public totalRecords = 0;
  public tourVisible = false;

  public orderKey = 'centroCusto.centroCusto';
  public order = 1;

  public meses = {
    'Janeiro': 'Janeiro',
    'Fevereiro': 'Fevereiro',
    'Marco': 'Março',
    'Abril': 'Abril',
    'Maio': 'Maio',
    'Junho': 'Junho',
    'Julho': 'Julho',
    'Agosto': 'Agosto',
    'Setembro': 'Setembro',
    'Outubro': 'Outubro',
    'Novembro': 'Novembro',
    'Dezembro': 'Dezembro'
  };

  public mesesValue = {
    'Janeiro': 1,
    'Fevereiro': 2,
    'Marco': 3,
    'Abril': 4,
    'Maio': 5,
    'Junho': 6,
    'Julho': 7,
    'Agosto': 8,
    'Setembro': 9,
    'Outubro': 10,
    'Novembro': 11,
    'Dezembro': 12
  };

  constructor(
    private store: StoreService,
    private helper: TableComponentHelper,
    private event: EventService,
    private storeApi: StoreApi,
    private action: ActionService,
    private toast: MessageService
  ) { }

  public toggleOrderBy(key){
    this.order = -1 * this.order;
    this.orderKey = key;

    this.orderBy(key);
  }

  public toggleOrderByMes(key){
    this.order = -1 * this.order;
    this.orderKey = key;

    this.orderByMes(key);
  }

  public orderByMes(key?, order?){
    key = key || this.orderKey;

    this.items = this.items.sort((a, b) => {
      const keyA = this.mesesValue[this.get(a, key)];
      const keyB = this.mesesValue[this.get(b, key)];

      if (keyA > keyB) {
        return this.order;
      }
      if (keyA < keyB) {
        return this.order * -1;
      }
      // a deve ser igual a b
      return 0;
    });
  }


  public orderBy(key?, order?){
    key = key || this.orderKey;

    this.items = this.items.sort((a, b) => {
      if (this.get(a, key) > this.get(b, key)) {
        return this.order;
      }
      if (this.get(a, key) < this.get(b, key)) {
        return this.order * -1;
      }
      // a deve ser igual a b
      return 0;
    });
  }

  private get(item, key){
    try{
      return eval(`item.`+key)
    }catch(e){
      return null;
    }
  }

  public openItemTour(){
    this.tourVisible = true;
  }

  private clone(obj){
    return JSON.parse(JSON.stringify(obj));
  }

  public closeTour(){
    this.store.changeTourState({ 'favorite': true });
    this.tourVisible = false;
  }

  public async ngOnInit() {
    this.store.source.subscribe(store => {        
      // reseta valor
      this.columns = {};

      // muda para página do store
      const rows = store.filtros.size;
      const page = store.filtros.page;

      this.rows = rows;
      this.first = rows * (page);

      store.colunasSelecionadas.forEach(item => {
        this.columns[item] = true;
      });

      this.items = this.clone((store.comprometidos.content || []))
        .map(item => this.helper.mapValorSaldo(item))
        .map(item => this.helper.mapStatus(item));

      this.totalComprometido = this.calcTotalComprometido(store.graficoTotal);
      this.totalRealizado = this.calcTotalRealizado(store.graficoTotal);
      this.totalSaldo = this.calcTotalSaldo(store.graficoTotal);

      const comprometidos: any = store.comprometidos;

      this.totalRecords = comprometidos.totalElements;

      this.orderBy();
    });
  }

  private isEquals(obj1, obj2){
    return JSON.stringify(obj1) == JSON.stringify(obj2);
  }

  private calcTotalComprometido(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorComprometido
      , 0);
  }

  private calcTotalRealizado(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorRealizado
      , 0);
  }

  private calcTotalSaldo(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorComprometido - item.valorRealizado
      , 0);
  }

  public openModalEdit(comprometido) {
    const modal = document.querySelector('#modal-edit');
    modal.classList.add('visible');
    this.event.onModalEditOpen.emit(comprometido);
  }

  public openModalRemove(comprometido) {
    const modal = document.querySelector('#modal-remove');
    modal.classList.add('visible');
    this.event.onModalDeleteOpen.emit(comprometido);
  }

  public paginate(e){
    const page = e.page;
    const size = e.rows;

    this.store.changeFilter({ page, size });

    this.storeApi.getComprometidos();
  }

  public async toggleFavorite(item){
    const isFav = item.isFav ? 0 : 1;
    
    try{
      await this.action.toggleFavorite(item.centroCusto, isFav);
    }catch(e){
      this.toast.add({ 
        severity: 'error', 
        detail: messages['msg.favorite.fail'] 
      });
    }
    
  }

}
