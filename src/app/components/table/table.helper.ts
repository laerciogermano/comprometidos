import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableComponentHelper {
  constructor() { }

  public parseTable(data) {
    return data.map(e => ({
      centroCusto: e.centroCusto.centroCusto,
      nomeAgente: e.nomeAgente,
      valorComprometido: e.valorComprometido,
      valorRealizado: e.valorRealizado,
      valorSaldo: e.valorComprometido - e.valorRealizado,
      status: this.parseStatus(e.status),
      favorito: e.favorito
    }));
  }

  private parseStatus(status) {
    console.log('-->', status);
    if (status == 'Comprometido')
      return 'comp';
    else if (status == 'Realizado')
      return 'real'
    else
      return 'parc';
  }

  public mapValorSaldo(item){
    item.valorSaldo = item.valorComprometido - item.valorRealizado;
    return item;
  }

  public mapStatus(item) {
    item.status = this.parseStatus(item.status);
    return item;
  }

}
