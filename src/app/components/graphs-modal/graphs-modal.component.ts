import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';

declare const Chart: any;

@Component({
  selector: 'app-graphs-modal',
  templateUrl: './graphs-modal.component.html',
  styleUrls: ['./graphs-modal.component.scss']
})
export class GraphsModalComponent implements OnInit {
  public state = 'resume';
  public totalComprometido;
  public totalRealizado;
  public totalSaldo;
  public totalRealizadoK;
  public pRealizado;
  public saldoPorProjeto;

  private colors = [];
  private graficoTotal;
  private graficoMes;
  private filtros;

  private barChart;
  private polarChart;

  public messages = {
    '>101': {
      title: 'Atenção!',
      content: 'Os valores executados estão acima do esperado',
      showIcon: false
    },
    '80-100': {
      title: 'Parabéns!',
      content: 'A execução orçamentária está dentro do planejado.',
      showIcon: true
    },
    '50-79': {
      title: 'Atenção!',
      content: 'A realização dos valores comprometidos está abaixo do esperado.',
      showIcon: false
    },
    '0-49': {
      title: 'Alerta!',
      content: 'A execução orçamentária está baixa, reveja sua previsão para um melhor controle orçamentário.',
      showIcon: false
    }
  }

  public message;

  private meses = [
    'Janeiro',
    'Fevereiro',
    'Marco',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
  ];

  constructor(
    private store: StoreService
  ) { }

  public showResume() {
    this.state = 'resume';
  }

  public showChart() {
    this.state = 'chart';
  }

  ngOnInit() {
    this.geraCores();

    this.store.source.subscribe(store => {
      let graficoTotal = store.graficoTotal || [];
      let graficoMes = store.graficoMes || [];

      graficoTotal = this.setCentroCustoColors(graficoTotal);

      this.totalComprometido = this.calcTotalComprometido(graficoTotal);
      this.totalRealizado = this.calcTotalRealizado(graficoTotal);
      this.totalSaldo = this.calcTotalSaldo(graficoTotal);
      this.totalRealizadoK = this.nFormatter(this.totalRealizado, 1);
      this.pRealizado = this.calcPorcentagemRealizado(this.totalComprometido, this.totalRealizado) || 0;

      this.saldoPorProjeto = this.calcSaldoPorProjeto(graficoTotal);

      if (!this.isEquals(this.graficoMes, graficoMes) || !this.isEquals(this.graficoTotal, graficoTotal)) {
        this.mountBarChart(graficoTotal, graficoMes);
        this.mountPorlarChart(graficoTotal);

        this.graficoTotal = graficoTotal;
        this.graficoMes = graficoMes;
      }

      // seta mensagem
      if (this.pRealizado <= 49) {
        this.message = this.messages['0-49'];
      } else if (this.pRealizado <= 79) {
        this.message = this.messages['50-79'];
      } else if (this.pRealizado <= 100) {
        this.message = this.messages['80-100'];
      } else {
        this.message = this.messages['>101'];
      }
    })
  }

  private isEquals(obj1, obj2) {
    return JSON.stringify(obj1) == JSON.stringify(obj2);
  }

  private setCentroCustoColors(graficoTotal) {
    return graficoTotal.map((e, i) => {
      e.color = this.colors[i];
      return e;
    });
  }

  private geraCores() {
    for (let i = 0; i <= 500; i++)
      this.colors[i] = this.getRandomColor();
  }

  private clone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  private calcSaldoPorProjeto(graficoTotal) {
    return graficoTotal.map(e => {
      return {
        centroCusto: e.centroCusto.centroCusto,
        saldo: (e.valorComprometido - e.valorRealizado) || 0,
        color: e.color
      }
    })
      .filter(e => e.saldo > 0);
  }

  private calcPorcentagemRealizado(totalComprometido, totalRealizado) {
    const percent = ((totalRealizado * 100) / totalComprometido) || 0;

    return (percent > 0 && percent < 1)
      ? percent.toFixed(2)
      : Math.floor(percent);
  }

  private calcTotalComprometido(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorComprometido
      , 0);
  }

  private nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
  }

  private calcTotalRealizado(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorRealizado
      , 0);
  }

  private calcTotalSaldo(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorComprometido - item.valorRealizado
      , 0);
  }


  private mountBarChart(graficoTotal, graficoMes) {
    console.log('montando grafico', graficoMes, graficoTotal);

    if (this.barChart)
      this.barChart.destroy();

    const parent = document.querySelector('.graph-bar .body');
    const oldChart: any = document.getElementById('bar-chart');

    if (oldChart) {
      oldChart.remove();
    }

    const newChart = document.createElement('canvas');
    newChart.id = 'bar-chart';

    parent.appendChild(newChart);

    const ctx2 = newChart.getContext("2d");

    const datasets = [];

    graficoTotal.forEach((e, i) => {
      const centroCusto = e.centroCusto.centroCusto;
      const dataComprometidos = [];
      const dataRealizados = [];

      this.meses.forEach((mes, mesI) => {
        const grafico = graficoMes.find(elem =>
          elem.mesRef == mes && elem.centroCusto.centroCusto == centroCusto
        ) || {};

        let saldo = grafico.valorComprometido - grafico.valorRealizado;

        if(saldo < 0)
          saldo = 0;

        if(grafico.valorRealizado < 0)
          grafico.valorRealizado = 0;
        
        dataComprometidos.push( saldo || 0);
        dataRealizados.push(grafico.valorRealizado || 0);

      });

      datasets.push({
        label: centroCusto + ' - Realizado',
        backgroundColor: '#4EC63A',
        data: dataRealizados,
        stack: i
      })

      datasets.push({
        label: centroCusto + ' - Saldo',
        backgroundColor: '#E8E8E8',
        data: dataComprometidos,
        stack: i
      })
    });

    var data = {
      labels: this.meses,
      datasets: datasets
    };

    this.barChart = new Chart(ctx2, {
      type: 'groupableBar',
      data: data,
      options: {
        responsive: true,
        legend: {
          position: 'top',
          display: false
        },
        title: {
          display: false,
          text: ''
        },
        scales: {
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  mountPorlarChart(graficoTotal) {
    const ctx: any = document.getElementById('chart');
    const ctx2 = ctx.getContext('2d');

    const array = graficoTotal
      .filter(e => e.valorComprometido > e.valorRealizado)

    const labels = array.map(e => e.centroCusto.centroCusto);

    const data = array.map(e => {
      const percent: any = this.calcPorcentagemRealizado(e.valorComprometido, e.valorRealizado);
      const diff = 100 - percent;

      return diff;
    });

    const colors = array.map(e => e.color);

    if (this.polarChart)
      this.polarChart.destroy();

    const config = {
      data: {
        datasets: [{
          data: data,
          backgroundColor: colors
        }],
        labels: labels
      },
      options: {
        responsive: false,
        legend: {
          position: 'bottom',
          display: false
        },
        title: {
          display: false,
          text: 'Chart.js Polar Area Chart'
        },
        scale: {
          ticks: {
            beginAtZero: true
            // stepSize: 1
          },
          // reverse: false
        },
        animation: {
          animateRotate: false,
          animateScale: true
        }
      }
    };

    this.polarChart = Chart.PolarArea(ctx2, config);
  }

  private getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  public closeModal() {
    const modal = document.querySelector('#modal-graph');
    modal.classList.remove('visible');
  }

}
