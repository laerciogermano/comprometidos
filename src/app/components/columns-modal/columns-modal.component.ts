import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { EventService } from 'src/app/services/Event';

@Component({
  selector: 'app-columns-modal',
  templateUrl: './columns-modal.component.html',
  styleUrls: ['./columns-modal.component.scss']
})
export class ColumnsModalComponent implements OnInit {
  public visible = false;
  public options = [];
  public selected = [];

  public value = true;
  constructor(
    private store: StoreService,
    private event: EventService
  ) { }

  ngOnInit() {
    this.options = this.clone(this.store.source.value.colunas);

    this.event.onModalColumnOpen.subscribe(() => {
      this.resetDefaultColumns();
      this.visible = true;
    });
    
    this.event.onModalColumnClose.subscribe(() => {
      this.visible = false;
    });
  }

  private resetDefaultColumns(){
    this.selected = this.clone(this.store.source.value.colunasSelecionadas);
  }

  public closeModal(){
    this.event.onModalColumnClose.emit();
  }

  public saveColumns(){
    this.store.changeSelectedColumns(this.selected);
    this.event.onModalColumnClose.emit();
  }

  private clone(obj){
    return JSON.parse(JSON.stringify(obj));
  }

}
