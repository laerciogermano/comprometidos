import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { StoreApi } from 'src/app/services/StoreApi';
import { ActionService } from 'src/app/services/ActionService';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  public current = 'todos';
  
  constructor(
    private storeApi: StoreApi,
    private store: StoreService,
    private action: ActionService
  ) { }

  ngOnInit() {
    this.store.source.subscribe(store => {
      this.current = store.currentTab;
    });
  }

  public activeTab(tab){
    this.current = tab;

    this.action.changeTab(tab);
  }

}
