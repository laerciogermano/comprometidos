import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { ApiService } from 'src/app/services/ApiService';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent{
  constructor(
    private api: ApiService,
    private store: StoreService
  ){}

  public openGraphs(){
    const modal = document.querySelector('#modal-graph');
    modal.classList.add('visible');
    console.log(modal);
  }

  public async downloadExcel(){
    try{
      const filters = this.store.source.value.filtros;

      const res: any = await this.api.downloadExcel(filters);

      const text = await res.text();

      this.downloadFile(res);
    }catch(e){
      alert('erro');
      console.log(e);
    }
  }

  downloadFile(data){
    var url = window.URL.createObjectURL(new Blob([data]));

     var a = document.createElement('a');
     document.body.appendChild(a);
     a.setAttribute('style', 'display: none');
     a.href = url;
     a.download = 'Resumo.xlsx';
     a.click();
     window.URL.revokeObjectURL(url);
     a.remove();
   }
  
  private download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}

