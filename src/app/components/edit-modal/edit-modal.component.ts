import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/ApiService';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { StoreService } from 'src/app/services/StoreService';
import { MessageService } from 'primeng/api';
import * as messages from "../../constants/Messages.json";
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import { StoreApi } from 'src/app/services/StoreApi';
import { EventService } from 'src/app/services/Event';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class EditModalComponent implements OnInit {
  public form;
  public tipos = [];

  public mesesRef = [
    { name: 'Janeiro' },
    { name: 'Fevereiro' },
    { name: 'Março' },
    { name: 'Abril' },
    { name: 'Maio' },
    { name: 'Junho' },
    { name: 'Julho' },
    { name: 'Agosto' },
    { name: 'Setembro' },
    { name: 'Outubro' },
    { name: 'Novembro' },
    { name: 'Dezembro' },
  ];

  public numberMask = createNumberMask({
    prefix: 'R$ ',
    suffix: '', // This will put the dollar sign at the end, with a space.
    allowDecimal: true,
    decimalSymbol: ',',
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: '.'
  });

  public submited = false;

  public maskDoc;
  public maskCPF = '999.999.999-99';
  public maskCNPJ = '99.999.999/9999-99';
  
  public result = [];

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private store: StoreService,
    private toast: MessageService,
    private storeApi: StoreApi,
    private event: EventService
  ) { }

  ngOnInit() {
    this.store.source.subscribe(store => {
      store = this.clone(store);

      // Map resolve problema de n aparecer a opção no dropdown
      this.tipos = store.tiposDespesas.map(e => ({
        codTipo: e.codTipo,
        dscTipo: e.dscTipo,
        dscTipoAbrev: e.dscTipoAbrev,
      }));
    });

    this.form = this.formBuilder.group({
      centroCusto: new FormControl(null, Validators.required),
      codComprometido: new FormControl(),
      tipoSistema: new FormControl(null, Validators.required),
      tipoDoc: new FormControl('1', Validators.required),
      doc: new FormControl(null, Validators.required),
      mes: new FormControl(null, Validators.required),
      valorComprometido: new FormControl(null, Validators.required),
      nomeAgente: new FormControl('', Validators.required),
      valorRealizado: new FormControl(''),
      observacao: new FormControl('')
    });

    this.setMaskDoc(this.form.value);

    this.form.valueChanges.subscribe(val => this.setMaskDoc(val));

    this.event.onModalEditOpen.subscribe(item => {
      this.submited = false;

      // Resolve problema de n aparecer opção no dropdown
      item.tipoSistema = {
        codTipo: item.tipoSistema.codTipo,
         // tem que ficar uppercase para aparecer no dropdown
        dscTipo: item.tipoSistema.dscTipo.toLowerCase(),
        dscTipoAbrev: item.tipoSistema.dscTipoAbrev,
      };

      console.log(item);

      this.form.patchValue(Object.assign(item, {
        mes: { name: item.mesRef == 'Marco' ? 'Março' : item.mesRef }
      }));

      // bug do prime ng
      setTimeout(() => {
        const doc = this.form.get('doc') as FormControl;
        
        doc.setValue(item.doc);
      });
    });
  }



  private parseDoc(doc){
    console.log(doc);
    return doc;
    // return '05909420348';
  }

  private setMaskDoc(val) {
    this.maskDoc = val.tipoDoc == '2'
      ? this.maskCPF
      : this.maskCNPJ;
  }

  private clone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  public async search(event) {
    try{
      const query = event.query;
      const ano = this.store.source.value.ano;

      const res: any = await this.api.buscaCentroCusto(ano, query);

      this.result = res || [];
    }catch(e){
      this.result = [];
    }
  }


  public closeModal() {
    const modal = document.querySelector('#modal-edit');
    // document.body.style.overflow = 'hidden';
    modal.classList.remove('visible');
    this.form.reset();
  }

  public async submit() {
    this.submited = true;

    if (!this.form.valid)
      return this.toast.add({ severity: 'error', detail: messages['msg.empty.fields'] });

    try {
      const values = this.form.value;

      await this.api.atualizarComprometido({
        centroCusto: values.centroCusto,
        tipoSistema: values.tipoSistema,
        codComprometido: values.codComprometido,
        tipoDoc: values.tipoDoc,
        doc: values.doc,
        nomeAgente: values.nomeAgente,
        mesRef: values.mes.name,
        observacao: values.observacao,
        valorComprometido: values.valorComprometido,
        valorRealizado: values.valorRealizado,
        codTipo: values.tipoSistema.codTipo,
        ano: this.store.value.ano,
        codAgente: this.store.value.codAgente
      });

      this.storeApi.getComprometidos();

      this.toast.add({
        severity: 'success',
        detail: messages['msg.update.success']
      });

      this.form.reset();
      this.closeModal();
    } catch (e) {
      this.toast.add({ severity: 'error', detail: messages['msg.update.fail'] });
      console.log(e);
    }
  }

  private parseCurrency(value){
    return parseFloat(new String(value)
      .replace(/R\$\s/, '')
      .replace(/\./, '')
      .replace(/\,/, '.')
    );
  }
}
