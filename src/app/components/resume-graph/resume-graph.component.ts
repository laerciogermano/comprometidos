import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';

@Component({
  selector: 'app-resume-graph',
  templateUrl: './resume-graph.component.html',
  styleUrls: ['./resume-graph.component.scss']
})
export class ResumeGraphComponent implements OnInit {
  public pRealizado;
  public dashArray = '0,100';

  constructor(
    private store: StoreService
  ) {}

  ngOnInit() {
    this.store.source.subscribe(store => {
      const totalComprometido = this.calcTotalComprometido(store.graficoTotal);
      const totalRealizado = this.calcTotalRealizado(store.graficoTotal);

      this.pRealizado = this.calcPorcentagemRealizado(totalComprometido, totalRealizado);

      this.dashArray = `${this.pRealizado},100`;    
    });
  }

  private calcPorcentagemRealizado(totalComprometido, totalRealizado){
    const percent = ((totalRealizado * 100) / totalComprometido) || 0;

    return (percent > 0 && percent < 1)
      ? percent.toFixed(2)
      : Math.floor(percent);
  }

  private calcTotalComprometido(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorComprometido
      , 0);
  }

  private calcTotalRealizado(graficoTotal) {
    graficoTotal = graficoTotal || [];

    return graficoTotal.reduce((total, item) =>
      total += item.valorRealizado
      , 0);
  }

}
