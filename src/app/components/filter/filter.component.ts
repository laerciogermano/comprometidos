import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/StoreService';
import { StoreApi } from 'src/app/services/StoreApi';
import { FormBuilder, FormControl } from '@angular/forms';
import { MessageService } from 'primeng/api';
import * as messages from "../../constants/Messages.json";
import { ApiService } from 'src/app/services/ApiService';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  public form;
  private filtros;

  public visible = false;

  public projetosOpts = [];

  public projeto;

  public anosRefs = []

  public statusOpts = [{
    label: 'Comprometido',
    value: 2
  }, {
    label: 'Parcial',
    value: 3
  }, {
    label: 'Realizado',
    value: 1
  }];

  public status;

  public tiposOpts = [];

  public tipo;

  public items = [];

  public mesesRefs = [
    { value: 'Janeiro' },
    { value: 'Fevereiro' },
    { value: 'Março' },
    { value: 'Abril' },
    { value: 'Maio' },
    { value: 'Junho' },
    { value: 'Julho' },
    { value: 'Agosto' },
    { value: 'Setembro' },
    { value: 'Outubro' },
    { value: 'Novembro' },
    { value: 'Dezembro' },
  ];

  constructor(
    private store: StoreService,
    private storeApi: StoreApi,
    private formBuilder: FormBuilder,
    private toast: MessageService,
    private api: ApiService
  ) { }


  ngOnInit() {
    this.form = this.formBuilder.group({
      centroCusto: new FormControl(),
      status: new FormControl(),
      tipo: new FormControl(),
      responsavel: new FormControl(),
      solicitante: new FormControl(),
      anoRef: new FormControl(),
      mesRef: new FormControl(),
      fornecedor: new FormControl(),
      cnpj: new FormControl(),
      observacao: new FormControl()
    });

    this.setValuesFromStore(this.store.source.value);

    let timeout;
    this.form.valueChanges.subscribe(val => {
      val = this.parseToStore(val);

      clearTimeout(timeout);

      timeout = setTimeout(async () => {
        console.log(val);
        this.storeApi.changeFilter(val);
      }, 400);
    })
  }

  

  private parseFromStore(filtros) {
    const anoRef = filtros.anoRef && { value: filtros.anoRef };
    const mesRef = filtros.mesRef && { value: filtros.mesRef };
    // const tipo   = filtros.tipo && { value: filtros.tipo };
    // const status = filtros.status && { value: filtros.status };

    return {
      centroCusto: filtros.centroCusto,
      status: filtros.status,
      tipo: filtros.tipo,
      responsavel: filtros.responsavel,
      solicitante: filtros.solicitante,
      anoRef: anoRef,
      mesRef: mesRef,
      fornecedor: filtros.fornecedor,
      cnpj: filtros.cnpj,
      observacao: filtros.observacao
    }
  }


  public async buscaCentroCusto(event) {
    try{
      const query = event.query;
      const ano = this.store.source.value.ano;

      const res: any = await this.api.buscaCentroCusto(ano, query);
      
      this.projetosOpts =  res || [];
    }catch(e){
      this.projetosOpts = [];
    }
  }


  private setValuesFromStore(store){
    store = this.clone(store);

    this.projetosOpts = this.parseProjetosAcoes(store.projetosAcoes);
    this.tiposOpts = this.parseTiposDespesas(store.tiposDespesas);
    this.anosRefs = this.parseAnosRefs(store.anosRefs);

    this.filtros = this.parseFromStore(store.filtros);

    this.form.patchValue(this.filtros);
  }

  private parseToStore(val){
    if(val.anoRef)
      val.anoRef = val.anoRef.value;

    if(val.mesRef)
      val.mesRef = val.mesRef.value;

    if(val.cnpj)
      val.cnpj = val.cnpj.replace(/[^\d]/g, '');

    if(val.centroCusto && val.centroCusto.length > 0)
      val.centroCusto = val.centroCusto.map(e => e.centroCusto);

    return val;
  }

  private isEqual(obj1, obj2) {
    return JSON.stringify(obj1) == JSON.stringify(obj2);
  }

  private clone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  private parseAnosRefs(data) {
    return data.map(value => ({ value }));
  }

  private parseProjetosAcoes(data) {
    return data.map(e => ({
      label: e.centroCusto + ' - ' + e.descCentroCusto,
      value: e.centroCusto
    }));
  }

  private parseTiposDespesas(data) {
    return data.map(e => ({
      label: e.dscTipo,
      value: e.codTipo
    }));
  }

  public toggleFilter() {
    this.visible = !this.visible;
  }

  public onInputChange(e) {}

}
