import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FilterComponent } from './components/filter/filter.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TableComponent } from './components/table/table.component';
import { ResumeComponent } from './components/resume/resume.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { BannerComponent } from './components/banner/banner.component';
import { ActionsComponent } from './components/actions/actions.component';
import { CreateModalComponent } from './components/create-modal/create-modal.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ColumnsModalComponent } from './components/columns-modal/columns-modal.component';
import { CheckboxModule } from 'primeng/checkbox';
// import { ChartModule } from 'primeng/chart';
import { GraphsModalComponent } from './components/graphs-modal/graphs-modal.component';
import { EditModalComponent } from './components/edit-modal/edit-modal.component';
import { RemoveModalComponent } from './components/remove-modal/remove-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { InputMaskModule } from 'primeng/inputmask';
import { HomeComponent } from './pages/home/home.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TextMaskModule } from 'angular2-text-mask';
import ptBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import {PaginatorModule} from 'primeng/paginator';
import { LoadingComponent } from './components/loading/loading.component';
import { CurrencyMaskModule, CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask";
import { ResumeGraphComponent } from './components/resume-graph/resume-graph.component';

registerLocaleData(ptBr)

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "left",
  allowNegative: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterComponent,
    TabsComponent,
    TableComponent,
    ResumeComponent,
    BannerComponent,
    ActionsComponent,
    CreateModalComponent,
    ColumnsModalComponent,
    GraphsModalComponent,
    EditModalComponent,
    RemoveModalComponent,
    HomeComponent,
    LoadingComponent,
    ResumeGraphComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MultiSelectModule,
    FormsModule,
    DropdownModule,
    RadioButtonModule,
    CheckboxModule,
    // ChartModule,
    HttpClientModule,
    InputMaskModule,
    InputTextareaModule,
    ToastModule,
    AutoCompleteModule,
    TextMaskModule,
    PaginatorModule,
    CurrencyMaskModule
  ],
  providers: [
    MessageService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
