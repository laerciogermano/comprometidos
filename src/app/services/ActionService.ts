import { Injectable } from '@angular/core';
import { StoreService } from './StoreService';
import { StoreApi } from './StoreApi';
import { ApiService } from './ApiService';
import { Config } from '../constants/Config';

/**
 * Exemplo de Action para comportamento
 * reativo unificado.
 */

@Injectable({
  providedIn: 'root'
})
export class ActionService {


  constructor(
    private storeApi: StoreApi,
    private store: StoreService,
    private api: ApiService
  ) {}


  public changeTab(tab){

    if(tab == 'favoritos'){
      this.store.changeFilter({isFav: 1, page: 0});
    }else{
      this.store.changeFilter({isFav: 0, page: 0});
    }

    this.store.changeCurrentTab(tab);
    this.storeApi.getComprometidos();
  }

  public openAdvancedFilters(){}

  public selectColumns(){}
  
  public changeFilter(){}

  public paginate(){}

  public async toggleFavorite(centroCusto, isFav){
    try{
      this.setFavComprometidos(centroCusto, isFav);

      const codAgente = this.store.source.value.codAgente;
      
      if(isFav)
        await this.api.favoritar(codAgente, centroCusto)
      else
        await this.api.desfavoritar(codAgente, centroCusto)

      this.storeApi.getComprometidos();
    }catch(e){
      this.setFavComprometidos(centroCusto, !isFav);
      throw e;
    }
  }

  private setFavComprometidos(centroCusto, isFav){
    let comprometidos: any = this.store.source.value.comprometidos;

    comprometidos.contente = (comprometidos.content || []).map(e => {
      try{
        if(e.centroCusto.centroCusto == centroCusto.centroCusto)
          e.isFav = isFav;
      }catch(e){}

      return e;
    });

    this.store.changeComprometidos(comprometidos);
  }

  public logout() {
    localStorage.clear();
    window.location.href = Config.URL_LOGIN;
  }
}
