import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from './TokenService.js';
import { StoreService } from './StoreService.js';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private headers;
  private headersSAS;
  private excelHeaders;

  private url = 'https://apihml.pr.sebrae.com.br/comprometido-api';

  constructor(
    public readonly http: HttpClient,
    public tokenService: TokenService,
    public store: StoreService
  ) {}

  public setHeaders(token){
    this.headers =  new HttpHeaders()
      .append('Authorization', token)
      .append('APP_KEY', 'a1096889-2787-4255-80f3-c79ce2a3b336') 
      .append('Content-Type', 'application/json');

    this.excelHeaders =  new HttpHeaders()
      .append('Authorization', token)
      .append('APP_KEY', 'a1096889-2787-4255-80f3-c79ce2a3b336')
      .append('Content-Type', 'attachment;filename=myfilename.csv');
    
    this.headersSAS =  new HttpHeaders()
      .append('Authorization', token)
      .append('APP_KEY', 'ff14205f-5a71-4c1a-9893-811b6158f5f9') 
      .append('Content-Type', 'application/json');

  }

  public async getComprometidos(filtros) {
    filtros = this.clone(filtros);

    const meses = {
      'Janeiro': 1,
      'Fevereiro': 2,
      'Março': 3,
      'Abril': 4,
      'Maio': 5,
      'Junho': 6,
      'Julho': 7,
      'Agosto': 8,
      'Setembro': 9,
      'Outubro': 10,
      'Novembro': 11,
      'Dezembro': 12
    };

    if(filtros.mesRef)
      filtros.mesRef = meses[filtros.mesRef];
  
    // ,1,2 => null,1,2
    filtros.status = (filtros.status || [])
      .map(e => ( e == null ? 'null' : e ));
    
    const parsed = this.clearNulls(filtros);
    const params = new URLSearchParams(parsed);
    const query = params.toString();

    return this.http.get(`${this.url}/comprometido/listAll?${query}`, { 
      headers: this.headers 
    }).toPromise();
  }

  public async downloadExcel(filtros) {
    filtros = this.clone(filtros);

    const meses = {
      'Janeiro': 1,
      'Fevereiro': 2,
      'Março': 3,
      'Abril': 4,
      'Maio': 5,
      'Junho': 6,
      'Julho': 7,
      'Agosto': 8,
      'Setembro': 9,
      'Outubro': 10,
      'Novembro': 11,
      'Dezembro': 12
    };

    if(filtros.mesRef)
      filtros.mesRef = meses[filtros.mesRef];

    // ,1,2 => null,1,2
    filtros.status = (filtros.status || [])
      .map(e => ( e == null ? 'null' : e ))
      
    const parsed = this.clearNulls(filtros);
    const params = new URLSearchParams(parsed);
    const query = params.toString();

    return this.http.get(`${this.url}/comprometido/export?${query}`, { 
      headers: this.excelHeaders,
      responseType: 'blob' 
    }).toPromise();
  }

  public async getGraficoTotal(filtros) {
    filtros = this.clone(filtros);

    const meses = {
      'Janeiro': 1,
      'Fevereiro': 2,
      'Março': 3,
      'Abril': 4,
      'Maio': 5,
      'Junho': 6,
      'Julho': 7,
      'Agosto': 8,
      'Setembro': 9,
      'Outubro': 10,
      'Novembro': 11,
      'Dezembro': 12
    };

    if(filtros.mesRef)
      filtros.mesRef = meses[filtros.mesRef];
    
    // ,1,2 => null,1,2
    filtros.status = (filtros.status || [])
      .map(e => ( e == null ? 'null' : e ));

    // filtros.codAgente = 87165;
    // filtros.codUnidade = 17;
    // filtros.anoRef = 2019;
      
    const parsed = this.clearNulls(filtros);
    const params = new URLSearchParams(parsed);
    const query = params.toString();

    return this.http.get(`${this.url}/grafico/total?${query}`, { 
      headers: this.headers 
    }).toPromise();
  }

  public async getGraficoMes(filtros) {
    filtros = this.clone(filtros);
    
    const meses = {
      'Janeiro': 1,
      'Fevereiro': 2,
      'Março': 3,
      'Abril': 4,
      'Maio': 5,
      'Junho': 6,
      'Julho': 7,
      'Agosto': 8,
      'Setembro': 9,
      'Outubro': 10,
      'Novembro': 11,
      'Dezembro': 12
    };

    if(filtros.mesRef)
      filtros.mesRef = meses[filtros.mesRef];
      
    // ,1,2 => null,1,2
    filtros.status = (filtros.status || [])
      .map(e => ( e == null ? 'null' : e ));

    // filtros.anoRef = 2019;
      
    const parsed = this.clearNulls(filtros);
    const params = new URLSearchParams(parsed);
    const query = params.toString();

    return this.http.get(`${this.url}/grafico/mes?${query}`, { 
      headers: this.headers 
    }).toPromise();
  }

  public async getFavoritos(codAgente) {
    return this.http.get(`${this.url}/favorito/listAll?codAgente=${codAgente}`, { 
      headers: this.headers 
    }).toPromise();
  }


  public clone(obj1){
    return JSON.parse(JSON.stringify(obj1))
  }

  public async deleteComprometidos(codAgente, codComprometido) {
    return this.http.delete(`${this.url}/comprometido?codAgente=${codAgente}&codComprometido=${codComprometido}`, { 
      headers: this.headers 
    }).toPromise();
  }

  public async getProjetosAcoes(anoRef, query) {
    return this.http.get(`${this.url}/util/listaCentroCusto?ano=${anoRef}&codCentroCusto=${query}`,{
      headers: this.headers
    }).toPromise();
  }

  public async getCodUnidade(codAgente) {
    return this.http.get(`${this.url}//usuario/unidade?codAgente=${codAgente}`,{
      headers: this.headers
    }).toPromise();
  }

  public async getTiposDespesas(codUnidade) {
    return this.http.get(`${this.url}/util/ListTipoSistema?codUnidade=${codUnidade}`, {
      headers: this.headers
    }).toPromise();
  }

  public async getAnosRefs() {
    return this.http.get(`${this.url}/util/anoRefAll`, {
      headers: this.headers
    }).toPromise();
  }

  public async buscaCentroCusto(ano, numero) {
    return this.http.get(`${this.url}/util/listaCentroCusto?ano=${ano}&codCentroCusto=${numero}`, {
      headers: this.headers
    }).toPromise();
  }

  public async criarComprometido(form) {
    return this.http.post(`${this.url}/comprometido/${form.codAgente}/${form.ano}`, JSON.stringify({	
      "centroCusto": form.centroCusto,
      "codComprometido": null,
      "tipoSistema": form.tipoSistema,
      "doc": form.doc.replace(/[^\w]/g, ''),
      "tipoDoc": form.tipoDoc,
      "nomeAgente": form.nomeAgente,
      "mesRef": form.mesRef,
      "observacao": form.observacao,
      "valorComprometido": form.valorComprometido,
      "valorRealizado": 0, // valor fixo, atualizado pelo siga
      "porcentagemRealizada": 0, // valor fixo atualizado pelo siga
      "observacaoAbrev": (form.observacao || '').slice(0, 10),
      "status": null,
      "codTipo": form.codTipo,
      "codProposta": null,
      "codPropStatus": ""
    }), {
      headers: this.headers
    }).toPromise();
  }

  public async atualizarComprometido(form) {
    return this.http.put(`${this.url}/comprometido/${form.codAgente}/${form.ano}`, JSON.stringify({	
      "centroCusto": form.centroCusto,
      "codComprometido": form.codComprometido,
      "tipoSistema": form.tipoSistema,
      "doc": form.doc.replace(/[^\d]/g, ''),
      "tipoDoc": form.tipoDoc,
      "nomeAgente": form.nomeAgente,
      "mesRef": form.mesRef,
      "observacao": form.observacao,
      "valorComprometido": form.valorComprometido,
      "valorRealizado": form.valorRealizado,
      // "porcentagemRealizada": 0, // valor fixo atualizado pelo siga
      "observacaoAbrev": (form.observacao || '').slice(0, 10),
      "status": null,
      "codTipo": form.codTipo,
      "codProposta": null,
      "codPropStatus": ""
    }), {
      headers: this.headers
    }).toPromise();
  }

  public async desfavoritar(codAgente, data) {
    const url = `${this.url}/favorito/${codAgente}`;
    const headers = this.headers;

    return this.http.put(url,  JSON.stringify(data), { headers })
      .toPromise();
  }

  public async favoritar(codAgente, data) {
    const url = `${this.url}/favorito/${codAgente}`;
    const headers = this.headers;

    return this.http.post(url, JSON.stringify([ data ]), { headers })
      .toPromise();
  }

  public async getUsuario() {
    const url = 'http://apihml.pr.sebrae.com.br/sas-api/public/mobile/login';
    const headers = this.headersSAS;
    
    return this.http.get(url, { headers })
      .toPromise();
  }
  
  public async getImagemUsuario(login){
    const src = `http://apihml.pr.sebrae.com.br/sas-api/usuario/foto-perfil/${login}`;

    const options = {
      headers: {
        'APP_KEY': 'ff14205f-5a71-4c1a-9893-811b6158f5f9',
        'Authorization': 'bf0a4276-d0b6-42d8-bac3-2c3cf7b0643b'
      }
    };

    return fetch(src, options)
    .then(res => res.blob())
    .then(blob => URL.createObjectURL(blob));
  }

  

  private sleep(time) {
    return new Promise(resolve => {
      setTimeout(resolve, time)
    });
  }

  // utils
  private clearNulls(obj){
    const cloned = Object.assign({}, obj);

    for(let i in cloned){
      if(cloned[i] == null)
          delete cloned[i];
    }

    return cloned;
  }
}
