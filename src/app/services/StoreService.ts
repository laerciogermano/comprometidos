import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  /**
   * - filtros;
   *    - projetoAção;
   *    - status;
   *    - tipoDespesa;
   *    - responsavel;
   *    - solicitante;
   *    - ano;
   *    - mes;
   *    - fornecedor;
   *    - cnpj;
   *    - observação;
   * - tabs;
   * - tabSelecionado;
   * - help;
   */

  public value = {
    ano: null,
    codAgente: null,
    codUnidade: null,
    token: null,
    usuario: {
      avatar: null
    },
    colunas: [
      { label: 'Centro de custo', value: 'centroCusto' },
      { label: 'Mês', value: 'data' },
      { label: 'Fornecedor', value: 'fornecedor' },
      { label: 'Comprometido', value: 'comprometido' },
      { label: 'Realizado', value: 'realizado' },
      { label: 'Saldo', value: 'saldo' },
      { label: 'Tipo de despesa', value: 'tipoDespesa' },
      { label: 'Tipo de contratação', value: 'detalhes' },
      { label: 'Solicitante', value: 'solicitante' }, // Não tem na api
      { label: 'Responsável', value: 'responsavel' },
      { label: 'CNPJ/CPF', value: 'ata' },
      { label: 'Status', value: 'status' },
      { label: 'Observação', value: 'observacao' }
      // { label: 'Tipo de lançamento', value: 'tipoLancamento' },
    ],
    colunasSelecionadas: [
      'centroCusto',
      'data',
      'fornecedor',
      'comprometido',
      'realizado',
      'saldo',
      'tipoDespesa',
      'detalhes',
      'solicitante',
      'responsavel',
      'ata',
      'status',
      'observacao'
    ],
    filtros: {
      codUnidade: null,
      codAgente: null,
      centroCusto: [],
      status: [],
      responsavel: null,
      solicitante: null,
      anoRef: null,
      mesRef: null,
      tipo: [], // tipoDespesa
      fornecedor: null,
      detalhes: null,
      cnpj: null,
      observacao: null,
      size: 10, // default values
      page: 0,  // default values
      paging: true, // default values
      sortDirection: 'DESC', // default values
      isFav: 0 // default values
    },
    comprometidos: {
      content: null
    },
    projetosAcoes: [],
    tiposDespesas: [],
    modais: {
      colunas: false
    },
    anosRefs: [],
    loading: false,
    currentTab: 'todos',
    storage: {
      tour: {
        column: false,
        favorite: false
      }
    },
    graficoTotal: null,
    graficoMes: null
  }

  public source = new BehaviorSubject(this.value);
  public data = this.source.asObservable();

  constructor() {
    this.retrieveStorage();
    this.persistStorageChanges();
  }

  private retrieveStorage() {
    try {
      const json: any = localStorage.getItem('storage');

      if (json !== null) {
        const storage = JSON.parse(json);

        this.source.value.storage = Object.assign({}, this.value.storage, storage);
        this.source.next(this.source.value);
      }

    } catch (e) { }
  }


  private persistStorageChanges() {
    this.source.subscribe(store => {
      console.log(store);
      localStorage.setItem('storage', JSON.stringify(store.storage));
    });
  }

  public changeToken(data) {
    this.source.value.token = data;
    this.source.next(this.source.value);
  }

  public changeUsuario(data) {
    this.source.value.usuario = data;
    this.source.next(this.source.value);
  }

  public changeImagemUsuario(data) {
    this.source.value.usuario.avatar = data;
    this.source.next(this.source.value);
  }

  public changeAno(data) {
    this.source.value.ano = data;
    this.source.value.filtros.anoRef = data;
    this.source.next(this.source.value);
  }

  public changeCodAgente(data) {
    this.source.value.codAgente = data;
    this.source.value.filtros.codAgente = data;
    this.source.next(this.source.value);
  }

  public changeCodUnidade(data) {
    this.source.value.codUnidade = data;
    this.source.value.filtros.codUnidade = data;
    this.source.next(this.source.value);
  }

  public changeComprometidos(data) {
    this.source.value.comprometidos = data;
    this.source.next(this.source.value);
  }

  public changeGraficoTotal(data) {
    this.source.value.graficoTotal = data;
    this.source.next(this.source.value);
  }

  public changeGraficoMes(data) {
    this.source.value.graficoMes = data;
    this.source.next(this.source.value);
  }

  public changeFilter(data) {
    this.source.value.filtros = Object.assign(this.source.value.filtros, data);
    this.source.next(this.source.value);
  }

  public changeTourState(data) {
    this.source.value.storage.tour = Object.assign(this.source.value.storage.tour, data);
    this.source.next(this.source.value);
  }

  public changeSelectedColumns(data) {
    this.source.value.colunasSelecionadas = data;
    this.source.next(this.source.value);
  }

  public changeProjetosAcoes(data) {
    this.source.value.projetosAcoes = data;
    this.source.next(this.source.value);
  }

  public changeTiposDespesas(data) {
    this.source.value.tiposDespesas = data;
    this.source.next(this.source.value);
  }

  public changeAnosRefs(data) {
    this.source.value.anosRefs = data;
    this.source.next(this.source.value);
  }

  public changeModalColuna(visible) {
    this.source.value.modais.colunas = visible;
    this.source.next(this.source.value);
  }

  public changeLoading(data) {
    this.source.value.loading = data;
    this.source.next(this.source.value);
  }

  public changeCurrentTab(data) {
    this.source.value.currentTab = data;
    this.source.next(this.source.value);
  }

}
