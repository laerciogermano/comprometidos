import { Injectable } from '@angular/core';
import { StoreService } from './StoreService';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private count = 0;

  constructor(
    private store: StoreService
  ){}

  public start() {
    const count = ++this.count;

    this.store.changeLoading(true);

    return count;
  }

  public stop(id) {
    setTimeout(() => {
      if(this.count == id)
        this.store.changeLoading(false);
    }, 600)
  }

}
