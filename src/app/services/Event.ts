import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  public onModalCreateOpen = new EventEmitter();
  public onModalColumnOpen = new EventEmitter();
  public onModalColumnClose = new EventEmitter();
  public onModalDeleteOpen = new EventEmitter();
  public onModalDeleteClose = new EventEmitter();
  public onModalEditOpen = new EventEmitter();
}
