import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor( ) { }

  public setLocalToken(token){
    localStorage.setItem('token', token);
  }

  public getLocalToken() {
    return localStorage.getItem('token');
  }

  public getQueryToken() {
    const params = new URLSearchParams(window.location.search);

    return params.get('token');
  }
}
