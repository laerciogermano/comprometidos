import { Injectable } from '@angular/core';
import { ApiService } from './ApiService';
import { StoreService } from './StoreService';
import { LoadingService } from './LoadingService';
import * as messages from "../constants/Messages.json";
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class StoreApi {
  private count = 0;

  constructor(
    private api: ApiService,
    private store: StoreService,
    private loading: LoadingService,
    private toast: MessageService
  ) { }

  public async getUsuario() {
    const id = this.loading.start();

    try {
      let usuario = await this.api.getUsuario();

      this.store.changeUsuario(usuario);
      this.loading.stop(id);

      return usuario;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.user.fail'] });
      throw e;
    }
  }

  public async getImagemUsuario(login) {
    const id = this.loading.start();
    try {
      let src = await this.api.getImagemUsuario(login);

      this.store.changeImagemUsuario(src);
      this.loading.stop(id);

      return src;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.user.image.fail'] });
      throw e;
    }
  }

  public changeFilter(filters){
    filters.page = 0;
    this.store.changeFilter(filters);
    this.getComprometidos();
  }

  public async getComprometidos() {
    const id         = this.loading.start();
    const codAgente  = this.store.value.codAgente;
    const codUnidade = this.store.value.codUnidade;
    const count      = ++this.count;
    const filtros    = this.store.source.value.filtros;

    filtros.codUnidade = codUnidade;
    
    try {
          
      let [
        comprometidos,
        favoritos,
        graficoTotal, 
        graficoMes
      ]: any = await Promise.all([
        this.api.getComprometidos(filtros),
        this.api.getFavoritos(codAgente),
        this.api.getGraficoTotal(filtros),
        this.api.getGraficoMes(filtros)
      ]);

      // caso a api n retorne
      comprometidos = comprometidos || {
        content: [],
        currentPage: 0,
        size: 0,
        totalElements: 0,
        totalPages: 0
      }

      // Tento resolver problema de prevalecer a última requisição
      if (this.count == count){
        comprometidos.content = (comprometidos.content || []).map(e => {
          e.isFav = !!favoritos.find(b => b.centroCusto == e.centroCusto.centroCusto && b.anoRef == filtros.anoRef);
          return e;
        });

        this.store.changeComprometidos(comprometidos || []);
        this.store.changeGraficoTotal(graficoTotal || []);
        this.store.changeGraficoMes(graficoMes || []);
      }
      
      this.loading.stop(id);

      return comprometidos;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.comprometidos.fail'] });
      throw e;
    }
  }

  private async getGraficos(){
    const filtros    = this.store.source.value.filtros;

    try{
      const [ graficoTotal, graficoMes ] = await Promise.all([
        this.api.getGraficoTotal(filtros),
        this.api.getGraficoMes(filtros)
      ]);

      this.store.changeGraficoTotal(graficoTotal || []);
      this.store.changeGraficoMes(graficoMes || []);
    }catch(e){
      this.toast.add({ severity: 'error', detail: messages['msg.grafico.fail'] });
      throw e;
    }
  }

  public async getProjetosAcoes(anoRef, query) {
    const id = this.loading.start();
    try {
      const res = await this.api.getProjetosAcoes(anoRef, query);

      this.store.changeProjetosAcoes(res || []);
      this.loading.stop(id);

      return res;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.projetoAcoes.fail'] });
      throw e;
    }
  }

  public async getTiposDespesas() {
    const id = this.loading.start();
    try {
      let codUnidade = this.store.source.value.codUnidade;
      let res: any = await this.api.getTiposDespesas(codUnidade);


      // Transforma em lowercase para caber melhor 
      // no front
      res = (res || []).map(e => {
        e.dscTipo = e.dscTipo.toLowerCase();
        return e;
      })

      this.store.changeTiposDespesas(res || []);
      this.loading.stop(id);

      return res;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.tiposDespesas.fail'] });
      throw e;
    }
  }

  public async getAnosRefs() {
    const id = this.loading.start();
    try {
      const res = await this.api.getAnosRefs();

      this.store.changeAnosRefs(res || []);
      this.loading.stop(id);

      return res;
    } catch (e) {
      this.loading.stop(id);
      this.toast.add({ severity: 'error', detail: messages['msg.anosRef.fail'] });
      throw e;
    }
  }

}
